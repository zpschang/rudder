pip install tensorflow-gpu==1.9.0
pip install --user gym visdom scipy tqdm joblib dill progressbar2 mpi4py cloudpickle click
pip install --user git+https://github.com/bioinf-jku/tensorflow-layer-library
python -m baselines.ppo2_rudder.run_atari --config baselines/ppo2_rudder/configs/Gravitar-1.json